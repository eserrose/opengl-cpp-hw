#include <iostream>
#include <vector>

#include "GLM/glm.hpp"
#include "GLM/common.hpp"
#include "GLM/gtc/type_ptr.hpp"
#include "GLAD/glad.h"
#include "GLFW/glfw3.h"

#include "opengl_utilities.h"
#include "mesh_generation.h"

#define SEGMENTS 16
#define CLEAR_VECTORS()	 \
			positions.clear();\
			normals.clear();\
			ids.clear();

#define ROTATION_SPEED	10
#define ROTATION_AXIS 	glm::vec3(1, 1, 0)
#define UP_LEFT			glm::vec3(-1, 1, 0)
#define DOWN_LEFT		glm::vec3(-1, -1, 0)
#define UP_RIGHT		glm::vec3(1, 1, 0)
#define DOWN_RIGHT		glm::vec3(1, -1, 0)
#define SCALE_FACTOR	0.5f

#define LEFT			glm::vec3(-1, 0, 0)
#define RIGHT			glm::vec3(1, 0, 0)
#define UP				glm::vec3(0, 1, 0)
#define DOWN			glm::vec3(0, -1, 0)

#define GRAY			glm::vec3(0.5, 0.5, 0.5)
#define RED				glm::vec3(1, 0, 0)
#define GREEN			glm::vec3(0, 1, 0)
#define BLUE			glm::vec3(0, 0, 1)

#define DEGREE(x)		glm::radians(float(glfwGetTime() * x))

enum Scenes {
	WIREFRAME,
	NORMAL,
	BLINN_PHONG,
	COLORFUL,
	GAME,
	SURPRISE,
	NO_SCENES
};

int current_scene = WIREFRAME;

/* Keep the global state inside this struct */
static struct {
	glm::dvec2 mouse_position;
	glm::ivec2 screen_dimensions = glm::ivec2(960, 960);
} Globals;

/* GLFW Callback functions */
static void ErrorCallback(int error, const char* description)
{
	std::cerr << "Error: " << description << std::endl;
}

static void CursorPositionCallback(GLFWwindow* window, double x, double y)
{
	Globals.mouse_position.x = x;
	Globals.mouse_position.y = y;
}

static void WindowSizeCallback(GLFWwindow* window, int width, int height)
{
	Globals.screen_dimensions.x = width;
	Globals.screen_dimensions.y = height;

	glViewport(0, 0, width, height);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_Q && action == GLFW_PRESS)
		current_scene = WIREFRAME;
	else if (key == GLFW_KEY_W && action == GLFW_PRESS)
		current_scene = NORMAL;
	else if (key == GLFW_KEY_E && action == GLFW_PRESS)
		current_scene = BLINN_PHONG;
	else if (key == GLFW_KEY_R && action == GLFW_PRESS)
		current_scene = COLORFUL;
	else if (key == GLFW_KEY_T && action == GLFW_PRESS)
		current_scene = GAME;
	else if (key == GLFW_KEY_Y && action == GLFW_PRESS)
		current_scene = SURPRISE;


}

static void draw_mesh(VAO mesh) {
	glBindVertexArray(mesh.id);
	glDrawElements(GL_TRIANGLES, mesh.element_array_count, GL_UNSIGNED_INT, NULL);
}

static void use_transform(GLint loc, glm::vec3 dir, float factor, bool rotation) {

	glm::mat4 transform(1.0), origin(1.0);
	transform = glm::scale(origin, glm::vec3(factor));
	transform = glm::translate(transform, dir);
	if(rotation) transform = glm::rotate(transform, glm::radians(float(glfwGetTime() * ROTATION_SPEED)), ROTATION_AXIS);
	glUniformMatrix4fv(loc, 1, GL_FALSE, glm::value_ptr(transform));
}

glm::dvec2 get_mouse_normal() {

	auto mouse_norm = Globals.mouse_position / glm::dvec2(Globals.screen_dimensions);
	mouse_norm.y = 1. - mouse_norm.y;
	mouse_norm = mouse_norm * 2. - 1.;
	return mouse_norm;
}


int main(int argc, char* argv[])
{
	/* Set GLFW error callback */
	glfwSetErrorCallback(ErrorCallback);

	/* Initialize the library */
	if (!glfwInit())
	{
		std::cout << "Failed to initialize GLFW" << std::endl;
		return -1;
	}

	/* Create a windowed mode window and its OpenGL context */
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	GLFWwindow* window = glfwCreateWindow(
		Globals.screen_dimensions.x, Globals.screen_dimensions.y,
		"Egehan Sezer", NULL, NULL
	);
	if (!window)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	/* Move window to a certain position [do not change] */
	glfwSetWindowPos(window, 10, 50);
	/* Make the window's context current */
	glfwMakeContextCurrent(window);
	/* Enable VSync */
	glfwSwapInterval(1);

	/* Load OpenGL extensions with GLAD */
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		glfwTerminate();
		return -1;
	}

	/* Set GLFW Callbacks */
	glfwSetCursorPosCallback(window, CursorPositionCallback);
	glfwSetWindowSizeCallback(window, WindowSizeCallback);
	glfwSetKeyCallback(window, key_callback);

	/* Configure OpenGL */
	glClearColor(0, 0, 0, 1);
	glEnable(GL_DEPTH_TEST);

	/* Creating Meshes */
	std::vector<glm::vec3> positions, normals;
	std::vector<GLuint> ids;

	GenerateParametricShapeFrom2D(positions,normals,ids, ParametricHalfCircle, SEGMENTS, SEGMENTS);
	VAO Sphere(positions,normals,ids);
	CLEAR_VECTORS();
	GenerateParametricShapeFrom2D(positions, normals, ids, ParametricCircle, SEGMENTS, SEGMENTS);
	VAO Torus(positions, normals, ids);
	CLEAR_VECTORS();
	GenerateParametricShapeFrom2D(positions, normals, ids, SomeCurve, 3*SEGMENTS, 3*SEGMENTS);
	VAO Hourglass(positions, normals, ids);
	CLEAR_VECTORS();
	GenerateParametricShapeFrom2D(positions, normals, ids, StarCurve, 3 * SEGMENTS, 3 * SEGMENTS);
	VAO starmesh(positions, normals, ids);


	/* Creating Programs */
	GLuint Scene_0 = CreateProgramFromSources(
		R"VERTEX(
#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;

uniform mat4 u_transform;

out vec3 vertex_position;
out vec3 vertex_normal;

void main()
{
	gl_Position = u_transform * vec4(a_position, 1);
	vertex_normal =  vec3(u_transform * vec4(a_normal, 0));
    vertex_position = vec3(gl_Position);
}
		)VERTEX",

		R"FRAGMENT(
#version 330 core

out vec4 out_color;

void main()
{
	out_color = vec4(1, 1, 1, 1);
}
		)FRAGMENT");
	if (Scene_0 == NULL)
	{
		glfwTerminate();
		return -1;
	}

	//Normal vecs as color
	GLuint Scene_1 = CreateProgramFromSources(
		R"VERTEX(
#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;

uniform mat4 u_transform;

out vec3 vertex_position;
out vec3 vertex_normal;

out vec4 out_color;

void main()
{
	gl_Position = u_transform * vec4(a_position, 1);
	vertex_normal =  vec3(u_transform * vec4(a_normal, 0));
    vertex_position = vec3(gl_Position);

}
		)VERTEX",

		R"FRAGMENT(
#version 330 core

in vec3 vertex_normal;

out vec4 out_color;

void main()
{
	vec3 color = normalize(vertex_normal);
	out_color = vec4(color,1);
}
		)FRAGMENT");
	if (Scene_1 == NULL)
	{
		glfwTerminate();
		return -1;
	}

	//Bling-Phong
	GLuint Scene_2 = CreateProgramFromSources(
		R"VERTEX(
#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;

uniform mat4 u_transform;

out vec3 vertex_position;
out vec3 vertex_normal;

void main()
{
	gl_Position = u_transform * vec4(a_position, 1);
	vertex_normal =  vec3(u_transform * vec4(a_normal, 0));
    vertex_position = vec3(gl_Position);

}
		)VERTEX",

		R"FRAGMENT(
#version 330 core

in vec3 vertex_position;
in vec3 vertex_normal;

out vec4 out_color;

void main()
{
	vec3 color = vec3(0);

	vec3 surface_color	  = vec3(0.5, 0.5, 0.5);
	vec3 surface_position = vertex_position;
	vec3 surface_normal   = normalize(vertex_normal);

	vec3 ambient_color = vec3(0.5,0.5,0.5);
	color += ambient_color*surface_color;

	vec3 light_dir   = normalize(vec3(-1, -1, 1));
	vec3 to_light    = -light_dir;
	vec3 light_color = vec3(0.4,0.4,0.4);

	float diffuse_k = 1;
	float diffuse_intensity = max(0, dot(to_light, surface_normal));
	color += diffuse_k * diffuse_intensity * light_color * surface_color;

	vec3 view_dir = normalize(vec3(vertex_position.xy, -1) - surface_position);
	vec3 halfway_dir = normalize(view_dir + to_light);
	float specular_k = 1;
	float specular_intensity = pow(max(0, dot(halfway_dir, surface_normal)), 64);
	color += specular_k * specular_intensity * light_color;

	out_color = vec4(color, 1);
}
		)FRAGMENT");
	if (Scene_2 == NULL)
	{
		glfwTerminate();
		return -1;
	}

	//Blinn-Phong 2
	GLuint Scene_3 = CreateProgramFromSources(
		R"VERTEX(
#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;

uniform mat4 u_transform;

out vec3 vertex_position;
out vec3 vertex_normal;


void main()
{
	gl_Position = u_transform * vec4(a_position, 1);
	vertex_normal =  (u_transform * vec4(a_normal, 0)).xyz;
    vertex_position = gl_Position.xyz;
}
		)VERTEX",

		R"FRAGMENT(
#version 330 core

uniform vec2 u_mouse_position;
uniform vec3 surface_color;
uniform float shine;

in vec3 vertex_position;
in vec3 vertex_normal;

out vec4 out_color;


void main()
{
	vec3 color = vec3(0);

	vec3 surface_position = vertex_position;
	vec3 surface_normal   = normalize(vertex_normal);

	vec3 ambient_color = vec3(0.5,0.5,0.5);
	color += ambient_color*surface_color;

	vec3 light_dir   = normalize(vec3(-1, -1, 1));
	vec3 to_light    = -light_dir;
	vec3 light_color = vec3(0.4,0.4,0.4);

	float diffuse_k = 1;
	float diffuse_intensity = max(0, dot(to_light, surface_normal));
	color += diffuse_k * diffuse_intensity * light_color * surface_color;

	vec3 view_dir = normalize(vec3(vertex_position.xy, -1) - surface_position);
	vec3 halfway_dir = normalize(view_dir + to_light);
	float specular_k = 1;
	float specular_intensity = pow(max(0, dot(halfway_dir, surface_normal)), 64);
	color += specular_k * specular_intensity * light_color;

	vec3 point_light_position = vec3(u_mouse_position, -1);
	vec3 point_light_color    =  vec3(0.5,0.5,0.5);
	vec3 to_point_light		  = normalize(point_light_position - surface_position);

	diffuse_intensity = max(0, dot(to_point_light, surface_normal)); 
	color += diffuse_k * diffuse_intensity * point_light_color * surface_color;

	view_dir			= normalize(vec3(vertex_position.xy, -1) - surface_position);
	halfway_dir			= normalize(view_dir + to_point_light);
	specular_intensity  = pow(max(0, dot(halfway_dir, surface_normal)), shine);
	color += specular_k * specular_intensity * point_light_color;

	out_color = vec4(color, 1);
}
		)FRAGMENT");
	if (Scene_3 == NULL)
	{
		glfwTerminate();
		return -1;
	}

	//Game
	GLuint Scene_4 = CreateProgramFromSources(
		R"VERTEX(
#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;

uniform mat4 u_transform;

out vec3 vertex_position;
out vec3 vertex_normal;

void main()
{
	gl_Position = u_transform * vec4(a_position, 1);
	vertex_normal =  (u_transform * vec4(a_normal, 0)).xyz;
    vertex_position = gl_Position.xyz;
}
		)VERTEX",

		R"FRAGMENT(
#version 330 core

uniform vec3 surface_color;

in vec3 vertex_position;
in vec3 vertex_normal;

out vec4 out_color;

void main()
{
	vec3 color = vec3(0);

	vec3 surface_position = vertex_position;
	vec3 surface_normal   = normalize(vertex_normal);

	vec3 ambient_color = vec3(0.5,0.5,0.5);
	color += ambient_color*surface_color;

	vec3 light_dir   = normalize(vec3(-1, -1, 1));
	vec3 to_light    = -light_dir;
	vec3 light_color = vec3(0.4,0.4,0.4);

	float diffuse_k = 1;
	float diffuse_intensity = max(0, dot(to_light, surface_normal));
	color += diffuse_k * diffuse_intensity * light_color * surface_color;

	vec3 view_dir = normalize(vec3(vertex_position.xy, -1) - surface_position);
	vec3 halfway_dir = normalize(view_dir + to_light);
	float specular_k = 1;
	float specular_intensity = pow(max(0, dot(halfway_dir, surface_normal)), 64);
	color += specular_k * specular_intensity * light_color;

	out_color = vec4(color, 1);
}
		)FRAGMENT");
	if (Scene_4 == NULL)
	{
		glfwTerminate();
		return -1;
	}

	//Custom
	GLuint Scene_5 = CreateProgramFromSources(
		R"VERTEX(
#version 330 core

layout(location = 0) in vec3 a_position;
layout(location = 1) in vec3 a_normal;

uniform mat4 u_transform;


out vec3 vertex_position;
out vec3 vertex_normal;

void main()
{
	gl_Position = u_transform * vec4(a_position, 1);
	vertex_normal =  (u_transform * vec4(a_normal, 0)).xyz;
    vertex_position = gl_Position.xyz;
}
		)VERTEX",

		R"FRAGMENT(
#version 330 core

in vec3 vertex_position;
in vec3 vertex_normal;

uniform vec2 u_mouse_position;

out vec4 out_color;

void main()
{
	vec3 color = vec3(0);

	vec3 surface_color = vec3(0.6f, 0.8f, 1f);
	vec3 surface_position = vertex_position;
	vec3 surface_normal = normalize(vertex_normal);

	float ambient_k = 0.5;
	vec3 ambient_color = vec3(1,1,1);
	color += ambient_k*ambient_color*surface_color;

	vec3 light_direction = normalize(vec3(-1,-1,1));
	vec3 light_color = vec3(1,0.5,1);

	float diffuse_k = max(0, dot(light_direction, surface_normal));
	color += diffuse_k * light_color * surface_color;

	vec3 view_dir = normalize(vec3(vertex_position.xy, -1) - surface_position);
	vec3 halfway_dir = normalize(view_dir + light_direction);
	float specular_k = pow(max(0, dot(halfway_dir, surface_normal)), 3);
	color += specular_k * light_color*surface_color;

	vec3 point_light_position = vec3(u_mouse_position, -1);
	vec3 point_light_color    =  vec3(1,0,0);
	vec3 to_point_light		  = normalize(point_light_position - surface_position);

	diffuse_k = max(0, dot(to_point_light, surface_normal)); 
	color += diffuse_k * point_light_color * surface_color;

	view_dir			= normalize(vec3(vertex_position.xy, -1) - surface_position);
	halfway_dir			= normalize(view_dir + to_point_light);
	specular_k  = pow(max(0, dot(halfway_dir, surface_normal)), 90);
	color += specular_k  * point_light_color;

	out_color = vec4(color*(normalize(vertex_position)+0.1), 1);
}
		)FRAGMENT");
	if (Scene_5 == NULL)
	{
		glfwTerminate();
		return -1;
	}

	GLuint Scenes[NO_SCENES] = {Scene_0,Scene_1,Scene_2,Scene_3,Scene_4, Scene_5};
	glm::vec2 chasing_pos(0);
	

	/* Loop until the user closes the window */
	
	glm::mat4 transform(1.0);
	transform = glm::scale(transform, glm::vec3(0.3));
	int counts[4] = { 0,0,0,1 };
	std::vector<glm::vec3> directions = { DOWN };
	bool sw = true;
	while (!glfwWindowShouldClose(window))
	{
		/* Render here */
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(Scenes[current_scene]);

		auto u_transform_location = glGetUniformLocation(Scenes[current_scene], "u_transform");

		if (current_scene == WIREFRAME) {

			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

			use_transform(u_transform_location, UP_LEFT, SCALE_FACTOR, true);
			draw_mesh(Sphere);
			use_transform(u_transform_location, UP_RIGHT, SCALE_FACTOR, true);
			draw_mesh(Torus);
			use_transform(u_transform_location, DOWN_LEFT, SCALE_FACTOR, true);
			draw_mesh(Hourglass);
			use_transform(u_transform_location, DOWN_RIGHT, SCALE_FACTOR, true);
			draw_mesh(starmesh);

		}
		else {
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			if (current_scene == NORMAL || current_scene == BLINN_PHONG) {

				use_transform(u_transform_location, UP_LEFT, SCALE_FACTOR, true);
				draw_mesh(Sphere);
				use_transform(u_transform_location, UP_RIGHT, SCALE_FACTOR, true);
				draw_mesh(Torus);
				use_transform(u_transform_location, DOWN_LEFT, SCALE_FACTOR, true);
				draw_mesh(Hourglass);
				use_transform(u_transform_location, DOWN_RIGHT, SCALE_FACTOR, true);
				draw_mesh(starmesh);
			}
			else if (current_scene == COLORFUL) {

				auto mouse_norm		= get_mouse_normal();
				auto u_mouse_loc	= glGetUniformLocation(Scenes[current_scene], "u_mouse_position");
				glUniform2fv(u_mouse_loc, 1, glm::value_ptr(glm::vec2(mouse_norm)));

				auto color_position = glGetUniformLocation(Scenes[current_scene], "surface_color");
				auto shine_position = glGetUniformLocation(Scenes[current_scene], "shine");

				glUniform3fv(color_position, 1, glm::value_ptr(GRAY));
				glUniform1f(shine_position, 128.f);
				use_transform(u_transform_location, UP_LEFT, SCALE_FACTOR, true);
				draw_mesh(Sphere);
				glUniform3fv(color_position, 1, glm::value_ptr(RED));
				glUniform1f(shine_position, 32.f);
				use_transform(u_transform_location, UP_RIGHT, SCALE_FACTOR, true);
				draw_mesh(Torus);
				glUniform3fv(color_position, 1, glm::value_ptr(GREEN));
				glUniform1f(shine_position, 42.f);
				use_transform(u_transform_location, DOWN_LEFT, SCALE_FACTOR, true);
				draw_mesh(Hourglass);
				glUniform3fv(color_position, 1, glm::value_ptr(BLUE));
				glUniform1f(shine_position, 666.f);
				use_transform(u_transform_location, DOWN_RIGHT, SCALE_FACTOR, true);
				draw_mesh(starmesh);
			}
			else if (current_scene == GAME) {

				auto color_position = glGetUniformLocation(Scenes[current_scene], "surface_color");
				auto mouse_norm = get_mouse_normal();
				glm::vec2 mouse_pos(mouse_norm.x, mouse_norm.y);

				if(glm::distance(chasing_pos, mouse_pos) > 0.3 * 2)
					glUniform3fv(color_position, 1, glm::value_ptr(GREEN));
				else
					glUniform3fv(color_position, 1, glm::value_ptr(RED));

				use_transform(u_transform_location, glm::vec3(mouse_pos,0)/0.3f, 0.3, false);
				draw_mesh(Sphere);

				chasing_pos = glm::mix(mouse_pos, chasing_pos, 0.99);
				glUniform3fv(color_position, 1, glm::value_ptr(GRAY));
				use_transform(u_transform_location, glm::vec3(chasing_pos, 0) / 0.3f, 0.3, false);
				draw_mesh(Sphere);
			}
			else {

				for (int i = 0; i < directions.size(); i++) {
					int count;
					if (directions[i] == UP) count = 0;
					else if (directions[i] == RIGHT) count = 1;
					else if (directions[i] == LEFT) count = 2;
					else if (directions[i] == DOWN) count = 3;
					auto temp = glm::translate(transform, directions[i] * (float)counts[count]);
					temp = glm::rotate(temp, glm::radians(float(20 * glfwGetTime())), glm::vec3(0, 1, 0));
					temp = glm::rotate(temp, glm::radians(float(glfwGetTime() * 2)), glm::vec3(0, 0, 1));
					glUniformMatrix4fv(u_transform_location, 1, GL_FALSE, glm::value_ptr(temp));
					draw_mesh(starmesh);
				}

				if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
					transform = glm::translate(transform, LEFT / 10.f);
				}
				if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
					transform = glm::translate(transform, RIGHT / 10.f);
				}
				if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
					transform = glm::translate(transform, UP / 10.f);
				}
				if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
					transform = glm::translate(transform, DOWN / 10.f);
				}

				if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS && sw) {
					sw = false;
					int x = rand() % 100;
					if (x > 75) {
						directions.push_back(UP);
						counts[0]++;
					}
					else if (x > 50) {
						directions.push_back(RIGHT);
						counts[1]++;
					}
					else if (x > 25) {
						directions.push_back(LEFT);
						counts[2]++;
					}
					else{
						directions.push_back(DOWN);
						counts[3]++;
					}
				}
				if (glfwGetKey(window, GLFW_KEY_I) == GLFW_RELEASE && !sw) {
					sw = true;
				}


			}
		}


		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}